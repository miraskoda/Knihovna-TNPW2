﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Transform;

namespace DataAccess.Interface
{
    public interface IDaoBase<T>
    {
        IList<T> GetAll();
        Object Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetById(int id);

    }
}

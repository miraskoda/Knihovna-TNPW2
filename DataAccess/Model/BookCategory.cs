﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class BookCategory : IEntity
    {

        public virtual int Id { set; get; }

        public virtual String Name { set; get; }

        public virtual String Description { set; get; }


    }
}

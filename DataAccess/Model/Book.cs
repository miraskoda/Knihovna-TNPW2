﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Book : IEntity
    {
        

        [Required(ErrorMessage ="Jmeno je vyzadovano")]
        public virtual string Name { get; set; }

        [Required(ErrorMessage ="Autor je vyzadovan")]
        public virtual string Author { get; set; }

        [Required(ErrorMessage = "Rok je vyzadovan")]
        [Range(1900, 2100, ErrorMessage = "Rok muze byt od 1900 do 2100")]
        public virtual int PublishedYear { get; set; }

        public virtual int Id { get; set; }

        [AllowHtml]
        public virtual string Description { get; set; }

        public virtual String ImageName { get; set; }

        public virtual BookCategory Category { get; set; }







    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using DataAccess.Model;

namespace WebApplication1.Areas.Admin.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
       [ChildActionOnly]
        public ActionResult Index()
        {

            KnihovnaUserDao KnihovnaUserDao = new KnihovnaUserDao();
            KnihovnaUser knihovnaUser = KnihovnaUserDao.GetByLogin(User.Identity.Name);



            return View(knihovnaUser);
        }
    }
}
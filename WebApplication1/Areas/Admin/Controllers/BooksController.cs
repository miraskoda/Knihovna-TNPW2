﻿using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using WebApplication1.Class;

namespace WebApplication1.Areas.Admin.Controllers
{
    [Authorize]
    public class BooksController : Controller
    {
        // GET: Books
        public ActionResult Index(int? page)
        {

            int itemsOnPage = 1;
            int pg = page.HasValue ? page.Value : 1;
            int totalBooks;

            BookDao bookDao = new BookDao();
            IList<Book> books = bookDao.GetBookPaged(itemsOnPage, pg, out totalBooks);

            ViewBag.Pages = (int) Math.Ceiling((double) totalBooks / (double) itemsOnPage);
            ViewBag.CurrentPage = pg;
            ViewBag.Categories = new BookCategoryDao().GetAll();


            KnihovnaUser user = new KnihovnaUserDao().GetByLogin(User.Identity.Name);


            if (user.Role.Identificator == "ctenar")
            {
                if (Request.IsAjaxRequest())
                {
                    return PartialView("IndexCtenar",books);
                }
                return View("IndexCtenar", books);
             
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView(books);
            }

        return View(books);
        }

        public ActionResult Category(int Id)
        {
            IList<Book> books = new BookDao().GetBooksInCategoryId(Id);
            ViewBag.Categories = new BookCategoryDao().GetAll();

            KnihovnaUser user = new KnihovnaUserDao().GetByLogin(User.Identity.Name);
            if (user.Role.Identificator == "ctenar")
            {
                return View("IndexCtenar", books);
            }


            return View("Index", books);


        }


        public JsonResult SearchBooks(string query)
        {

            BookDao bookDao = new BookDao();
            IList<Book> books = bookDao.SearchBooks(query);

            List<string> names = (from Book b in books select b.Name).ToList();


            return Json(names, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Search(string phrase)
        {
            BookDao bookDao = new BookDao();
            IList<Book> books = bookDao.SearchBooks(phrase);

            KnihovnaUser user = new KnihovnaUserDao().GetByLogin(User.Identity.Name);

            if (user.Role.Identificator == "ctenar")
            {
                if (Request.IsAjaxRequest())               
                    return PartialView("IndexCtenar", books);
                
            return View("IndexCtenar", books);
            }

                if (Request.IsAjaxRequest())
                    return PartialView("Index", books);
            

            return View("Index", books);

        }



        public ActionResult Detail(int id)
        {

            BookDao bookDao = new BookDao();
            Book b = bookDao.GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(b);

            }


            return View(b);
        }



        [Authorize(Roles = "knihovnik")]
        public ActionResult Create()
        {
            BookCategoryDao bookCategoryDao = new BookCategoryDao();
            IList<BookCategory> categories = bookCategoryDao.GetAll();
            ViewBag.categories = categories;

       

            return View();
        }

        [Authorize(Roles = "knihovnik")]
        [HttpPost]
        public ActionResult Add(Book book, HttpPostedFileBase picture, int categoryId)
        {
            if (ModelState.IsValid)
            {

                if (picture != null)
                {

                    if (picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {

                        Image image = Image.FromStream(picture.InputStream);

                        if (image.Height > 200 || image.Width > 20)
                        {
                            Image smallImage = ImageHelpercs.ScaleImage(image, 200, 200);
                            Bitmap b = new Bitmap(smallImage);
                            Guid guid = Guid.NewGuid();
                            string imageName = guid.ToString() + ".jpg";

                            b.Save(Server.MapPath("~/uploads/book/" + imageName), ImageFormat.Jpeg);

                            smallImage.Dispose();
                            b.Dispose();

                            book.ImageName = imageName;
                        }
                        else
                        {
                            picture.SaveAs(Server.MapPath("~/uploads/book/" + picture.FileName));
                        }


                    }

                }


                BookCategoryDao bookCategoryDao = new BookCategoryDao();
                BookCategory bookCategory = bookCategoryDao.GetById(categoryId);

                book.Category = bookCategory;

                BookDao bookDao = new BookDao();
                bookDao.Create(book);



                TempData["message-success"] = "Kniha byla uspesne pridana";

            }
            else
            {
                return View("Create", book);
            }


            return RedirectToAction("Index");


        }

        public ActionResult Edit(int id)
        {
            BookDao bookDao = new BookDao();
            BookCategoryDao bookCategoryDao = new BookCategoryDao();

            Book b = bookDao.GetById(id);
            ViewBag.Categories = bookCategoryDao.GetAll();


            return View(b);

        }
        [HttpPost]
        [Authorize(Roles = "knihovnik")]
        public ActionResult Update(Book book, HttpPostedFileBase picture, int categoryId)
        {

            try
            {
                BookDao bookDao = new BookDao();
                BookCategoryDao bookCategoryDao = new BookCategoryDao();

                BookCategory bookCategory = bookCategoryDao.GetById(categoryId);
                book.Category = bookCategory;

                if (picture != null)
                {

                    if (picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {

                        Image image = Image.FromStream(picture.InputStream);
                        Guid guid = Guid.NewGuid();
                        string imageName = guid.ToString() + ".jpg";

                        if (image.Height > 200 || image.Width > 20)
                        {
                            Image smallImage = ImageHelpercs.ScaleImage(image, 200, 200);
                            Bitmap b = new Bitmap(smallImage);


                            b.Save(Server.MapPath("~/uploads/book/" + imageName), ImageFormat.Jpeg);

                            smallImage.Dispose();
                            b.Dispose();

                        }

                        else
                        {
                            picture.SaveAs(Server.MapPath("~/uploads/book/" + picture.FileName));
                        }

                        System.IO.File.Delete(Server.MapPath("~/uploads/book/" + book.ImageName));
                        book.ImageName = imageName;

                    }


                    bookDao.Update(book);

                    TempData["message-success"] = "Kniha byla upravena";
                }

            }

            catch (Exception)
            {
                throw;

            }


            return RedirectToAction("Index", "Books");

        }


        [Authorize(Roles = "knihovnik")]
        public ActionResult Delete(int id)
        {
            try
            {
                BookDao bookDao = new BookDao();
                Book book = bookDao.GetById(id);


                System.IO.File.Delete(Server.MapPath("~/uploads/book/" + book.ImageName));

                bookDao.Delete(book);

                TempData["message-success"] = "Kniha byla smazana";

            }
            catch (Exception exception)
            {
                throw;
            }



            return RedirectToAction("Index");

        }



    }
}
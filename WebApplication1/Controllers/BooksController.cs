﻿using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using WebApplication1.Class;

namespace WebApplication1.Controllers
{
   
    public class BooksController : Controller
    {
        // GET: Books
        public ActionResult Index()
        {
            BookDao bookDao = new BookDao();
            IList<Book> books = bookDao.GetAll();

            return View(books);
        }


        public ActionResult Detail(int id) {

               BookDao bookDao = new BookDao();
            Book b = bookDao.GetById(id);

            return View(b);
        }


    }
}